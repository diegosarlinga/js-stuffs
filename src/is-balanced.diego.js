function isBalanced(expression) {
    var expectedPile = [];

    // order of opening/closing brackets should match
    var openingBrackets = '{[(';
    var closingBrackets = '}])';
    var expressionLength = expression.length;
    var char;

    var bracketsMap = {};
    openingBrackets.split('').forEach(function(opening, index) {
        bracketsMap[opening] = closingBrackets[index];
    });

    function isOpening(char) {
        return ~openingBrackets.indexOf(char);
    }

    function isClosing(char) {
        return ~closingBrackets.indexOf(char);
    }

    for(var index = 0; index < expressionLength; index++) {
        char = expression[index];

        if (isOpening(char)) {
            expectedPile.push(bracketsMap[char]);
        }

        if (isClosing(char)) {
            if (expectedPile.length === 0) {
                console.error('Unexpected ' + char + ' at position ' + index + '. There is no matching opening');
                return false;
            }

            var expectedClosing = expectedPile.pop();
            if (char !== expectedClosing) {
                console.error('Unexpected: ' + char + ' at position ' + index + '. Expected: ' + expectedClosing);
                return false;
            }
        }
    }

    if (expectedPile.length > 0) {
        console.error('Unclosed openings found, expected: ' + expectedPile.join());
        return false;
    }

    return true;
}

module.exports = isBalanced;
