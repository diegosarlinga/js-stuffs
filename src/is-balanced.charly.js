function isBalanced(expression) {
  // Our "stack" which is just an array.
  var stack = [];

  // Used to determine the correct closer for the popped opener
  var pairs = {
    '{': '}',
    '[': ']',
    '(': ')',
  };

  // The reason for not filtering is we need the index for error reporting.
  expression.split('').forEach(function(brace, index){

    // Since index starts with zero, we increment to make sense.
    var position = index + 1;

    if(!~'({[)}]'.indexOf(brace)){

      // If it's not a brace, return early. It doesn't affect anything.
      // We just need to take them into account for positioning.
      return;

    } else if(~'({['.indexOf(brace)){

      // If it's an opening, push to the stack
      stack.push(brace);

    } else if(!stack.length){

      // We have exhausted the stack but we have a closer
      return false;

    } else if(~')}]'.indexOf(brace)){

      var braceToClose = stack.pop();
      var expectedCloser = pairs[braceToClose];

      // If there was a mismatch in closing
      if(brace !== expectedCloser){
        return false;
      }
    }
  });

  // If we still need closing, throw an error with the next brace to close
  if(stack.length){
    return false;
  }

  return true;
}

module.exports = isBalanced;
