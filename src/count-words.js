function countWords(text) {
    var wordRegex =/\b(\w+)\b/g;

    var words = text.match(wordRegex);

    var wordMap = words.reduce(function(map, word) {
        var key = word.toLowerCase();
        var count = map[key] || 0;
        map[key] = count + 1;
        return map;
    }, {});

    var sortedWords = Object.keys(wordMap).sort(function(wordA, wordB) {
        return wordMap[wordB] - wordMap[wordA];
    });

    function padRight(string, limit) {
        for (var index = string.length; index < limit; index ++) {
            string += ' ';
        }
        return string;
    }

    function padLeft(string, limit) {
        for (var index = string.length; index < limit; index ++) {
            string = ' ' + string;
        }
        return string;
    }

    sortedWords.forEach(function(word) {
        console.log(padRight(word, 20), padLeft(wordMap[word].toString(), 3));
    });
}

module.exports = countWords;
