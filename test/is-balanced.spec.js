var expect = require('chai').expect;
var isBalanced = require('../src/is-balanced.diego');

describe('GIVEN an isBalanced function', function() {
    it('THEN it should be defined', function() {
        expect(isBalanced).to.exist;
    });

    var testCases = [{
        expression: '',
        result: true
    }, {
        expression: '[(]',
        result: false
    }, {
        expression: ']',
        result: false
    }, {
        expression: '[()]',
        result: true
    }, {
        expression: '[(])',
        result: false
    }, {
        expression: '[{()}]({[]})[({})]',
        result: true
    }, {
        expression: '}[]{',
        result: false
    }, {
        expression: '[',
        result: false
    }];

    testCases.forEach(function(testCase) {
        describe('WHEN expression ' + testCase.expression + ' is evalueated', function() {
            it('THEN it should result ' + testCase.result, function() {
                expect(isBalanced(testCase.expression)).to.equal(testCase.result);
            });
        });

    });
});
