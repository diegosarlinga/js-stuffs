# JS Stuffs

## Getting started

### Clone repo

`git clone https://diegosarlinga@bitbucket.org/diegosarlinga/js-stuffs.git`

the spet into the project folder

`cd js-stuffs`

### Install dependencies

`npm install`

### Run tests

`npm test`

## Requirements

* Node js
* Git
* Test editor of your preference
